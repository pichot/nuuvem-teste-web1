class CreatePurchases < ActiveRecord::Migration[6.1]
  def change
    create_table :purchases do |t|
      t.integer :product_quantity

      t.references :customer, index: true
      t.references :product, index: true
      t.timestamps
    end
  end
end
