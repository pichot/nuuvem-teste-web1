class PurchasesController < ApplicationController
  before_action :set_purchase, only: %i[ show destroy ]

  def index
    @purchases = Purchase.all
  end

  def new
    @purchase = Purchase.new
  end

  def create
    if Purchase.create_from_file params[:txt_file]
      redirect_to purchases_url, notice: "Purchase was successfully created."
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    @purchase.destroy
    redirect_to purchases_url, notice: "Purchase was successfully destroyed."
  end

  private
    def set_purchase
      @purchase = Purchase.find(params[:id])
    end
end
