class FileParser < ApplicationService

    attr_reader :txt_file

    def initialize txt_file
        @txt_file = txt_file
    end

    def call
        parse_txt
    end

    private

    def parse_txt
        rows = @txt_file.read.split("\n")
        remove_header rows
        rows
    end

    def remove_header rows
        rows.shift
    end
    
end