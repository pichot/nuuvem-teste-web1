class Customer < ApplicationRecord
    has_many :purchases

    validates_presence_of :name
end
