class Purchase < ApplicationRecord    
    belongs_to :customer
    belongs_to :product

    def self.create_from_file file
        txt_file_rows = FileParser.call file

        txt_file_rows.each do |row|
            @instance = self.new
            @purchaser_name, @item_description, @item_price,
            @purchase_count, @merchant_address, @merchant_name = row.split "\t", 6

            self.setup_relations
            @instance.save!
        end
    end

    private

    def self.setup_relations
        customer = Customer.find_or_create_by(name: @purchaser_name)
        merchant = Merchant.find_or_create_by(name: @merchant_name, address: @merchant_address)
        product = Product.find_or_create_by(
            description: @item_description,
            price: @item_price,
            merchant: merchant)

        @instance.product_quantity = @purchase_count
        @instance.product = product
        @instance.customer = customer
    end

end
