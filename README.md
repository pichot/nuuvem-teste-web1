# README
# nuuvem-teste-web1

Rails App for admission test for nuuvem.

The app is a Rails 6 app created with `rails new` command using Postgres 11 Database.
It also comes with a Docker compose ready to build up if you want an isolated environment.

## Technologies:

* Docker-Compose (optional, build if you don't have ruby3 in your machine)
* Rails 6
* Ruby 3.0.1
* PostgresSql 11

### Setup with existing ruby enviroment

Once defined your ruby environment, run rake tasks to setup the database 

* `bundle exec rake db:create`
* `bundle exec rake db:migrate`

### Setup with Docker compose

The project contains one **Dockerfile** one **docker-compose.yml** and an **entrypoint.sh**, together they provide the command sequence to build and run the ruby environment and a postgres database. At startup the rails app and postgres Database are up and running without messing with your local environment.

You can refer to Docker official docs on how to install docker-compose [link to Docker docs!](https://docs.docker.com/compose/install/)

### Build and running the Docker-compose:

Some of projects file are created inside the containers due to this you need to set permissions of project files to your current user.
* `sudo chown -R $USER:$USER .`

After this lets build and UP our containers.
* `docker-compose up -d`

This will build up a ruby debian based image and install all project dependencies described in the Gemfile, it cant take some time...

Check the running containers via `docker ps` command

### Setup the database

With the containers running, you can execute command via `docker exec` and then run the rake tasks to setup the database

* docker exec -it web1-rails bundle exec rake db:create
* docker exec -it web1-rails bundle exec rake db:migrate

* ...

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...