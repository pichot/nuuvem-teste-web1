require "test_helper"

class ProductTest < ActiveSupport::TestCase
  setup do
    @product = products(:one)
  end

  test "Can't save a product without description" do
    @product.description = ''
    assert_not @product.valid?
  end

  test "Can't save a product without price" do
    @product.price = nil
    assert_not @product.valid?
  end

  test "Can't save a product without merchant" do
    @product.merchant = nil
    assert_not @product.valid?
  end

  test "Shouldn't save same product twice" do
    @product.save
    
    assert_no_difference('Product.count') do
      new_product = products(:one)
      new_product.save
    end
  end
end
