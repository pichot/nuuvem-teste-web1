require "test_helper"

class MerchantTest < ActiveSupport::TestCase
  setup do
    @merchant = merchants(:one)
  end

  test "Can't save a merchant without name" do
    @merchant.name = ''
    assert_not @merchant.valid?
  end

  test "Can't save a merchant without address" do
    @merchant.address = ''
    assert_not @merchant.valid?
  end

  test "Shouldn't save same merchant twice" do
    @merchant.save
    
    assert_no_difference('Merchant.count') do
      new_merchant = merchants(:one)
      new_merchant.save
    end
  end
end
