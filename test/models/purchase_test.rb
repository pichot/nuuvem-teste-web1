require "test_helper"

class PurchaseTest < ActiveSupport::TestCase
  setup do
    @purchase = purchases(:one)
  end

  test "Can't save a purchase without quantity" do
    @purchase.product_quantity = nil
    assert_not @purchase.valid?
  end

  test "Can't save a purchase without customer" do
    @purchase.customer = nil
    assert_not @purchase.valid?
  end

  test "Can't save a purchase without product" do
    @purchase.product = nil
    assert_not @purchase.valid?
  end
end
