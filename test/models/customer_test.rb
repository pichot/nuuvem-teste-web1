require "test_helper"

class CustomerTest < ActiveSupport::TestCase

  setup do
    @customer = customers(:one)
  end


  test "Can't save a customer without name" do
    @customer.name = ''
    assert_not @customer.valid?
  end

  test "Shouldn't save same customer twice" do
    @customer.save
    
    assert_no_difference('Customer.count') do
      new_customer = customers(:one)
      new_customer.save
    end
  end
end
