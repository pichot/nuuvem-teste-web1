require "test_helper"

class PurchasesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @purchase = purchases(:one)
  end

  def post_txt_file
    txt_blob = fixture_file_upload('example_input.tab')
    post purchases_url, params: { txt_file: txt_blob }
  end

  test "should get index" do
    get purchases_url
    assert_response :success
  end

  test "should get new" do
    get new_purchase_url
    assert_response :success
  end

  test "should create purchase" do  
    assert_difference('Purchase.count', 4) do
      post_txt_file
    end

    assert_redirected_to purchases_url
  end

  test "should report gross income in flash message" do
    post_txt_file
    assert_equal "Total gross income $75.00.", flash[:notice]
  end
  
  test "should destroy purchase" do
    assert_difference('Purchase.count', -1) do
      delete purchase_url(@purchase)
    end

    assert_redirected_to purchases_url
  end

end
