FROM ruby:3.0-slim-buster

RUN apt-get update -qq && apt-get install -y build-essential nodejs npm postgresql-client libpq-dev

WORKDIR /var/www/app/

COPY ./Gemfile* /var/www/app/
RUN bundle install
RUN npm install --global yarn
RUN rails webpacker:install
#COPY ../compose /var/www/app/

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]